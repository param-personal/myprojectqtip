#!/usr/bin/env python
import os
import sys
import unittest
import json

try:
    import utils.utilities as qUtils
    from qtipmeapi import QtipMeApi, QtipMeApiTestCase, QtipMeMemberApi
    from utils.defines import API, RavintolaNoutoateria
    import utils.shared_resources as resources
    import utils.apimethods as apiMethods
except ImportError:
    print ("Please make sure that you have set a proper pythonpath")
    try:
        print ("Current PYTHONPATH: %s" % str(os.environ['PYTHONPATH']))
    except KeyError:
        print ("Can't find PYTHONPATH. Hint: set python path to: server/tests")
    sys.exit(1)

qUtils.log_info("Entered file: %s" % __file__)

try:
    host = os.environ['TEST_HOST']
except KeyError:
    api = API("staging.wow-q.com")
    if qUtils.isWindows():
        host = api.getDefault()
    else:
        host = api.staging if qUtils.isLinux and os.environ['USER'] == 'jenkins' else api.getDefault()

qUtils.log_info("Running tests for host: %s" % host)


class TestCreateOrderNA(QtipMeApiTestCase):

    def setUp(self):
        QtipMeApiTestCase.setUp(self)
        self.fmsg = "Test case %s failed" % self.tcname
        self.office = RavintolaNoutoateria()
        self.office.initQueues()
        today = qUtils.getWeekday()
        self.username = self.office.getUsername()
        self.password = self.office.getPassword()
        # open office for accepting orders
        apiMethods.OpenCloseOffice(today, 0, self.username, self.password)
        # change office timings so that orders can be accepted
        apiMethods.ChangeOpenCloseOfficeTime(today, '16:00', '20:20', self.username, self.password)
        self.client = QtipMeApi("client/noutoateria/takeorder.php", host, False)
        self.get_params = {}
        self.testdata = {}

    def tearDown(self):
        self.setTcStatus()
        QtipMeApiTestCase.tearDown(self)

    def test01_TryWithNoData(self):
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test02_TryWithMissingUri(self):
        self.get_params = {'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test03_TryWithMissingUuid(self):
        self.get_params = {'uri' : 'testuri-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test04_TryWithMissingQueueId(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test05_TryWithMissingOfficeId(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test06_TryWithMissingDeviceType(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id,'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test07_TryWithMissingIsDeliveryFlag(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test08_TryWithInvalidQueueId(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : 'abcdef', 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test09_TryWithInvalidOfficeId(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : 'abcdef', 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test10_TryWithInvalidDeviceType(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 100, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test11_TryWithInvalidWantSMSFlag(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 'Yes', 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test12_TryWithInvalidForceLocalFlag(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 'Wrong value'}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test13_TryWithInvalidIsDeliveryFlag(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 'Yes', 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test14_TryWithMissingLat(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'is_delivery' : 1, 'device_type': 0, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test15_TryWithMissingLong(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test16_TryWithMissingName(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test17_TryWithMissingPhone(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test18_TryWithMissingAddress(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'email' : 'abcdef', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test19_TryWithMissingOrder(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcdef', 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test20_TryWithMissingLength(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}])}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test21_TryWithMismatchingLength(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 4}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test22_TryWithInvalidJson(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : [{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}], 'length' : 4}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test23_TryWithInactiveQueue(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[1].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test24_TryWithNonExistentOffice(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : 100, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test25_TryWithNonExistentQueue(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : 100, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    def test26_TryWithQueueNotBelongingToOffice(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : 1, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertNotEqual(self.response['status'], 0, self.fmsg)

    '''def test27_PlaceOrderSuccessfully(self):
        self.get_params = {'uri' : 'testuri-order', 'uuid' : 'testuuid-order', 'queue_id' : self.office.queues[2].queue_id, 'office_id' : self.office.queues[2].office_id, 'device_type': 0, 'is_delivery' : 1, 'lat' : 60.24, 'long' : 24.57, 'want_sms' : 0, 'force_local' : 0}
        self.testdata = {'name' : 'testuser', 'phone' : '+3581234567', 'address' : 'Koronakatu 1, Espoo', 'email' : 'abcd@qtip.me', 'order' : json.dumps([{"id":"1","units":"1","unit_price":"12.00","total_price":12,"owner":"8"}]), 'length' : 1}
        self.runTestScenario()
        self.assertEqual(self.response['status'], 0, self.fmsg)'''

    def runTestScenario(self, apiReturnToFmsg=True):
        self.client.setWriteAPI()
        #query parameters for GET
        self.client.addParams(**self.get_params)
        self.client.generateGetUrl()
        #query parameters for POST
        self.client.createPostParams(**self.testdata)
        self.post_result = self.client.doPostRequest()
        self.response = self.post_result.json()
        self.fmsg = "%s [%s]" % (self.fmsg, str(self.response)) if apiReturnToFmsg else self.fmsg

if __name__ == '__main__':
    unittest.main(verbosity=2)
